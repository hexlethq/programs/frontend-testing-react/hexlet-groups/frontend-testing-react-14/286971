const fc = require('fast-check');

const revertSortCallback = (a, b) => b - a;
const sortCallback = (a, b) => a - b;
const sort = (data) => data.slice().sort(sortCallback);

// BEGIN
describe('sort', () => {
  it('сортирует числа так же как эталонная sort', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.integer()),
        (array) => {
          expect(array.sort(sortCallback)).toStrictEqual(sort(array));
        },
      ),
    );
  });
  it('сортирует строки так же как эталонная sort', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.string()),
        (array) => {
          expect(array.sort(sortCallback)).toStrictEqual(sort(array));
        },
      ),
    );
  });
  it('сортирует числа с плавающей точкой как эталонная sort', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.float()),
        (array) => {
          expect(array.sort(sortCallback)).toStrictEqual(sort(array));
        },
      ),
    );
  });
  it('сортирует числа', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.float()),
        (array) => {
          expect(array.sort(sortCallback)).toBeSorted();
        },
      ),
    );
  });
  it('сортирует числа с плавающей точкой', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.float()),
        (array) => {
          expect(array.sort(sortCallback)).toBeSorted();
        },
      ),
    );
  });
  it('сортирует строки', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.string()),
        (array) => {
          expect(array.sort(sortCallback)).toBeSorted({ coerce: true });
        },
      ),
    );
  });
  it('сортирует в числа порядке строки', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.integer()),
        (array) => {
          expect(array.sort(revertSortCallback)).toBeSorted({ descending: true });
        },
      ),
    );
  });
  it('сортирует числа с плав. точкой порядке строки', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.float()),
        (array) => {
          expect(array.sort(revertSortCallback)).toBeSorted({ descending: true });
        },
      ),
    );
  });

  it('инвариантность - длина массива', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.integer()),
        (array) => {
          const sorted = array.sort(sortCallback);
          expect(sorted).toHaveLength(array.length);
        },
      ),
    );
  });
  it('инвариантность - каждый элемент исходного массива должен быть в отсортированном', () => {
    expect.hasAssertions();
    const findAllElements = (array1, array2) => {
      const notFoundElem = array1.some((arrayElement) => array2.indexOf(arrayElement) === -1);
      return !notFoundElem;
    };

    fc.assert(
      fc.property(
        fc.array(fc.integer()),
        (array) => {
          const sorted = array.sort(sortCallback);
          expect(findAllElements(sorted, array)).toBeTruthy();
        },
      ),
    );
  });
  it('идемпотентность', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.integer()),
        (array) => {
          const sorted = array.sort(sortCallback);
          expect(sorted.sort(sortCallback)).toStrictEqual(sorted);
        },
      ),
    );
  });
  it('идемпотентность числа с плавающей точкой', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.float()),
        (array) => {
          const sorted = array.sort(sortCallback);
          expect(sorted.sort(sortCallback)).toStrictEqual(sorted);
        },
      ),
    );
  });
  it('сортирует: предыдущий меньше или равен следующему', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.integer()),
        (array) => {
          const sorted = array.sort(sortCallback);
          for (let i = 1; i < sorted.length; i += 1) {
            expect(sorted[i - 1]).toBeLessThanOrEqual(sorted[i]);
          }
        },
      ),
    );
  });
  it('сортирует числа с плавающей точкой: предыдущий меньше или равен следующему', () => {
    expect.hasAssertions();
    fc.assert(
      fc.property(
        fc.array(fc.float()),
        (array) => {
          const sorted = array.sort(sortCallback);
          for (let i = 1; i < sorted.length; i += 1) {
            expect(sorted[i - 1]).toBeLessThanOrEqual(sorted[i]);
          }
        },
      ),
    );
  });
});
// END
