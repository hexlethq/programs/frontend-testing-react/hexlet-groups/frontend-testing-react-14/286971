describe('object.assign', () => {
  it('передаём {}, {} -> {}', () => {
    expect.hasAssertions();
    const src = {};
    const target = {};
    const result = Object.assign(target, src);
    const expected = {};
    expect(result).toStrictEqual(expected);
  });
  it('передаём {}, null, undefined -> {}', () => {
    expect.hasAssertions();
    const src = null;
    const src2 = undefined;
    const target = {};
    const result = Object.assign(target, src, src2);
    const expected = {};
    expect(result).toStrictEqual(expected);
  });
  it('передаём {}, 123, true -> {}', () => {
    expect.hasAssertions();
    const src = 123;
    const src2 = true;
    const target = {};
    const result = Object.assign(target, src, src2);
    const expected = {};
    expect(result).toStrictEqual(expected);
  });
  it('передаём {}, "123" -> {}', () => {
    expect.hasAssertions();
    const src = '123';
    const target = {};
    const result = Object.assign(target, src);
    const expected = { 0: '1', 1: '2', 2: '3' };
    expect(result).toStrictEqual(expected);
  });
  it('передан объект с символьным свойством -> новый объект с символьным свойством', () => {
    expect.hasAssertions();
    const src = { [Symbol('obj')]: 123 };
    const target = {};
    const result = Object.assign(target, src);
    const expected = { [Symbol('obj')]: 123 };
    expect(result).toMatchObject(expected);
  });
  it('переданы объекты с одноимёнными свойствами -> новый объект со значением свойства последнего', () => {
    expect.hasAssertions();
    const src = { name: 'Ivan' };
    const src2 = { name: 'Petr' };
    const target = {};
    const result = Object.assign(target, src, src2);
    const expected = { name: 'Petr' };
    expect(result).toStrictEqual(expected);
  });
  it('передан объект с наследуемым свойством -> {}', () => {
    expect.hasAssertions();
    const src = Object.create({ age: 123 });
    const target = {};
    const result = Object.assign(target, src);
    const expected = {};
    expect(result).toStrictEqual(expected);
  });
  it('передан объект с неперечисляемым свойством -> {}', () => {
    expect.hasAssertions();
    const src = Object.create({}, {
      name: {
        value: 'Ivan',
      },
    });
    const target = {};
    const result = Object.assign(target, src);
    const expected = {};
    expect(result).toStrictEqual(expected);
  });
  it('передан объект с перечисляемым свойством -> новый объект с этим свойстом', () => {
    expect.hasAssertions();
    const src = Object.create({}, {
      age: {
        value: 123,
        enumerable: true,
      },
    });
    const target = {};
    const result = Object.assign(target, src);
    const expected = { age: 123 };
    expect(result).toStrictEqual(expected);
  });
  it('передан целевой объект со свойстом для чтения, перезаписываем это свойсто -> исключение', () => {
    expect.hasAssertions();
    const src = Object.create({}, {
      name: {
        value: 'Pert',
        enumerable: true,
      },
    });
    const target = Object.defineProperty({}, 'name', {
      value: 'Ivan',
      writable: false,
    });
    expect(() => { Object.assign(target, src); }).toThrow('');
  });
  it('передан объект с геттером -> в новом объекте значение геттера', () => {
    expect.hasAssertions();
    const DATE = new Date('01.01.2020');
    const dateDifference = (date) => {
      const now = new Date();
      const msInYear = 1000 * 3600 * 24 * 365;
      return Math.floor(Math.abs(date.getTime() - now.getTime()) / (msInYear));
    };
    const src = {
      birthday: DATE,
      get age() {
        return dateDifference(this.birthday);
      },
    };
    const target = {};
    const result = Object.assign(target, src);
    const expected = { age: 1, birthday: DATE };
    expect(result).toStrictEqual(expected);
  });
  it('передали объект, изменили свойство на верхнем уровне -> в новом объекте значение не поменялось', () => {
    expect.hasAssertions();
    const src = {
      age: 123,
      fio: {
        name: 'Ivan',
      },
    };
    const target = {};
    const result = Object.assign(target, src);
    src.age = 12;
    const expected = {
      age: 123,
      fio: {
        name: 'Ivan',
      },
    };
    expect(result).toStrictEqual(expected);
  });
  it('передали объект, изменили свойство на глубоком уровне -> в новом объекте значение поменялось', () => {
    expect.hasAssertions();
    const src = {
      age: 123,
      fio: {
        name: 'Ivan',
      },
    };
    const target = {};
    const result = Object.assign(target, src);
    src.fio.name = 'Petr';
    const expected = {
      age: 123,
      fio: {
        name: 'Petr',
      },
    };
    expect(result).toStrictEqual(expected);
  });
  it('передали target, объект, изменили свойство у target -> в новом объекте значение поменялось', () => {
    expect.hasAssertions();
    const src = {
      age: 123,
      name: 'Ivan',
    };
    const target = {
      name: 'Petr',
      profession: 'frontend',
    };
    const result = Object.assign(target, src);
    const expectedText = 'QA';
    target.profession = expectedText;
    expect(result.profession).toBe(expectedText);
  });
});
