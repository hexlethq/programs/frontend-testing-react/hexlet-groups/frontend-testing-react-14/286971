const faker = require('faker');

// BEGIN
describe('faker/helpers/createTransaction', () => {
  it('-> есть результат', () => {
    expect.hasAssertions();
    const result = faker.helpers.createTransaction();
    const expected = expect.any(Object);
    expect(result).toStrictEqual(expected);
  });
  it('-> есть корректный результат', () => {
    expect.hasAssertions();
    const result = faker.helpers.createTransaction();
    expect(result).toStrictEqual({
      account: expect.stringMatching(/\d{8}/),
      amount: expect.stringMatching(/\d*.\d{2}/),
      business: expect.any(String),
      date: expect.any(Date),
      name: expect.any(String),
      type: expect.any(String),
    });
  });
  it('-> есть результат разный', () => {
    expect.hasAssertions();
    const result = faker.helpers.createTransaction();
    const expected = faker.helpers.createTransaction();
    expect(result).not.toStrictEqual(expected);
  });
});
// END
