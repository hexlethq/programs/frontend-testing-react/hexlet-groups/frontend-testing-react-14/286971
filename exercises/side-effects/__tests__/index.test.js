const fs = require('fs');

const {
  getCurrentVersion, setNewVersion, upVersion,
} = require('../src/index.js');

const VERSION_TYPE = {
  MAJOR: 'major',
  MINOR: 'minor',
  PATCH: 'patch',
};
// BEGIN
const VERSION = '1.3.2';
const FILE_NAME = 'package.json';

describe('src/index.js', () => {
  describe('upVersion', () => {
    afterEach(() => {
      setNewVersion(FILE_NAME, VERSION);
    });
    it('ничего не передали -> patch', () => {
      expect.hasAssertions();
      const expectedVersion = '1.3.3';
      const expected = {
        version: expectedVersion,
      };
      upVersion(FILE_NAME);
      expect(getCurrentVersion(FILE_NAME)).toStrictEqual(expected);
    });
    it.skip('передали другой файл -> patch в другом файле', () => {
      expect.hasAssertions();
      const expectFileName = 'new_package_version.json';
      const actualVersion = '2.0.0';
      const expectedVersion = '2.0.1';
      const expected = {
        version: expectedVersion,
      };
      upVersion(expectFileName);
      expect(getCurrentVersion(expectFileName)).toStrictEqual(expected);
      setNewVersion(expectFileName, actualVersion);
    });
    const MAP_VERSION = [
      [VERSION_TYPE.PATCH, '1.3.3'],
      [VERSION_TYPE.MINOR, '1.4.2'],
      [VERSION_TYPE.MAJOR, '2.3.2'],
    ];
    it.each(MAP_VERSION)('передали %s -> version: %s', (versionType, versionExpected) => {
      expect.hasAssertions();
      const expected = {
        version: versionExpected,
      };
      upVersion(FILE_NAME, versionType);
      expect(getCurrentVersion(FILE_NAME)).toStrictEqual(expected);
    });
  });

  it('getCurrentVersion', () => {
    expect.hasAssertions();
    jest.spyOn(fs, 'readFileSync').mockReturnValueOnce(JSON.stringify({ version: VERSION }));

    const expected = {
      version: VERSION,
    };
    expect(getCurrentVersion(FILE_NAME)).toStrictEqual(expected);
    jest.clearAllMocks();
  });

  describe('setNewVersion', () => {
    it('ничего не передаём -> null', () => {
      expect.hasAssertions();
      expect(setNewVersion(FILE_NAME)).toBeUndefined();
    });

    it('передали version -> null', () => {
      expect.hasAssertions();
      jest.spyOn(fs, 'writeFileSync').mockReturnValueOnce(JSON.stringify({ version: VERSION }));
      const expectedVersion = '1.3.2';
      const expected = {
        version: expectedVersion,
      };

      setNewVersion(FILE_NAME, expectedVersion);
      expect(fs.writeFileSync).toHaveBeenCalledTimes(1);
      expect(fs.writeFileSync).toHaveBeenCalledWith(
        '__fixtures__/package.json',
        JSON.stringify(expected),
        'utf8',
      );

      expect(fs.writeFileSync.mock.calls).toMatchSnapshot();
      jest.clearAllMocks();
    });
  });
});
// END
