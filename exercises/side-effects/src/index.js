const fs = require('fs');

const VERSION_TYPE = {
  MAJOR: 'major',
  MINOR: 'minor',
  PATCH: 'patch',
};

const getCurrentVersion = (fileName) => {
  const contentInFile = fs.readFileSync(`__fixtures__/${fileName}`, 'utf8');
  return JSON.parse(contentInFile);
};

const setNewVersion = (fileName = 'package.json', version) => {
  if (!version) {
    return;
  }
  const objVersion = {
    version,
  };
  const stringVersion = JSON.stringify(objVersion);
  fs.writeFileSync(`__fixtures__/${fileName}`, stringVersion, 'utf8');
};

const getNewVersion = (currentVersion = '', versionType = VERSION_TYPE.PATCH) => {
  if (!currentVersion) {
    return null;
  }
  let [major, minor, patch] = currentVersion.version.split('.');

  if (versionType === VERSION_TYPE.MAJOR) {
    major = +major + 1;
  }
  if (versionType === VERSION_TYPE.MINOR) {
    minor = +minor + 1;
  }
  if (versionType === VERSION_TYPE.PATCH) {
    patch = +patch + 1;
  }
  return [major, minor, patch].join('.');
};

// BEGIN
const upVersion = (packagePath = 'package.json', versionType = VERSION_TYPE.PATCH) => {
  const currentVersion = getCurrentVersion(packagePath);
  const newVersion = getNewVersion(currentVersion, versionType);
  setNewVersion(packagePath, newVersion);
};

// END

module.exports = {
  upVersion, getCurrentVersion, setNewVersion,
};
