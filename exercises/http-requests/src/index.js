const axios = require('axios');

// BEGIN
const get = async (url) => {
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const post = async (url, user) => {
  try {
    const resp = await axios.post(url, user);
    return resp.data;
  } catch (error) {
    throw error.response.data;
  }
};
// END

module.exports = { get, post };
