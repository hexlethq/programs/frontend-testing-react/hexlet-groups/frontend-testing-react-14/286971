const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
describe('http-requests', () => {
  describe('get', () => {
    it('get positive', async () => {
      expect.hasAssertions();
      const URL = 'https://example.com/users';
      const DATA = {
        result: [{
          firstname: 'Fedor',
          lastname: 'Sumkin',
          age: 33,
        }],
      };

      nock(URL).get(/.*/).reply(200, DATA);
      const result = await get(URL);
      expect(result).toStrictEqual(DATA);
    });
    it('get negative', async () => {
      expect.hasAssertions();
      const URL = 'https://example.com/users';
      const ERROR = {
        message: 'Failed',
      };

      nock(URL).get(/.*/).reply(422, ERROR);
      const result = () => get(URL);
      await expect(result()).rejects.toStrictEqual(ERROR);
    });
  });
  describe('post', () => {
    it('post positive', async () => {
      expect.hasAssertions();
      const URL = 'https://example.com/users';
      const DATA = {
        firstname: 'Fedor',
        lastname: 'Sumkin',
        age: 33,
      };

      nock(URL)
        .post('', DATA)
        .reply(201, DATA);
      const data = await post(URL, DATA);
      expect(data).toStrictEqual(DATA);
    });
    it('post negative', async () => {
      expect.hasAssertions();
      const URL = 'https://example.com/users';
      const DATA = {
        firstname: 'Fedor',
        lastname: 'Sumkin',
        age: 33,
      };
      const ERROR = {
        message: 'Failed',
      };

      nock(URL)
        .post('', DATA)
        .reply(422, ERROR);
      const result = () => post(URL, DATA);
      await expect(result()).rejects.toStrictEqual(ERROR);
    });
  });
});
// END
